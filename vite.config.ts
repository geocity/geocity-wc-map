import { defineConfig } from 'vite';
import basicSsl from '@vitejs/plugin-basic-ssl';

// https://vitejs.dev/config/
export default defineConfig(({ command, mode, ssrBuild }) => {
  return {
    build: {
      emptyOutDir: true,
      copyPublicDir: false,
      lib: {
        entry: 'src/openlayers-element.ts',
        fileName: 'geocity-wc-map',
        formats: ['es'],
      },
    },

    plugins: [basicSsl()],
  }
});
