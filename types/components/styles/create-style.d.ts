import { FeatureLike } from 'ol/Feature';
import { Style } from 'ol/style';
export default class CreateStyle {
    static setupCircles(feature: FeatureLike): Style[];
    static selectedStyle(): Style[];
    static unSelectedStyle(): Style[];
}
