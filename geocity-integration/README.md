
# Adding a hot reload of the goecity-wc-map component in the geocity dev instance



In the main `geocity` repo, add the following lines to your `.env` file:

```sh
# Path to the geocity-wc-map folder (to have geocity-wc-map dev autoreload)
WC_MAP_PATH="<path-to/geocity-wc-map>"

# Which docker-compose to load (ON PRODUCTION, USE ONLY docker-compose.yml !!)
# If you need to connect to thumbor image service, simply use:
# COMPOSE_FILE=docker-compose.yml:docker-compose.thumbor.yml
COMPOSE_FILE=docker-compose.yml:docker-compose.dev.yml:${WC_MAP_PATH}/geocity-integration/docker-compose.dev-wc-map.yml
```

# How it works

The principle is to add to the `geocity` docker compose stack a `wc-map-builder` docker container doing the `ts` -> `js` transpilation, and to mount the transpiled `js` in the geocity code. An import of the transpiled `geocity-ws-map` is then hacked into the `advanced-geometry-widget.js`.

Note that `advanced-geometry-widget.js` is a copy from the geocity repo (`geocity/assets/js/advanced-geometry-widget.js`) and should track change of the file over there.

(yes it's hacky but it works. improvements welcome :-) )
