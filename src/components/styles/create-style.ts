import { FeatureLike } from 'ol/Feature';
import { Fill, Stroke, Style } from 'ol/style';
import CircleStyle from 'ol/style/Circle';

export default class CreateStyle {
  static setupCircles(
    feature: FeatureLike
  ) {
    let style: Array<Style> = [];
    if (feature.get('isSelected')) {
      return this.selectedStyle();
    } else {
      return this.unSelectedStyle();
    }
    return style;
  }

  static selectedStyle() {
    return [
      new Style({
        zIndex: 1,
        image: new CircleStyle({
          radius: 8,
          stroke: new Stroke({
            color: '#FFFFFF',
            width: 3,
          }),
          fill: new Fill({
            color: 'rgb(239, 68, 68, 0.75)',
          }),
        }),
      }),
    ];
  }

  static unSelectedStyle() {
    return [
      new Style({
        zIndex: 1,
        image: new CircleStyle({
          radius: 6,
          stroke: new Stroke({
            color: '#FFFFFF',
            width: 1,
          }),
          fill: new Fill({
            color: 'rgb(239, 68, 68, 0.75)',
          }),
        }),
      }),
    ];
  }
}
